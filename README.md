**免费开源网校系统源代码轻松搭建在线教育平台**

技术交流QQ群：468278040</br>

官网:[http://www.inxedu.com](http://http://www.inxedu.com)</br>

演示站:[http://demo1.inxedu.com](http://http://demo1.inxedu.com)</br>

账号:demo@inxedu.com</br>

密码:111111</br>
</br>
网站功能模块:</br>

课程功能</br>

咨询功能</br>

问答功能</br>

首页banner推荐</br>

支付宝支付功能集成</br>

播放模块</br>

个人中心模块</br>

个人资料模块</br>

修改头像模块</br>

收藏课程模块</br>

问题总结:
项目导入如果get set报错请添加lombok插件就可以正常使用了</br>

**技术框架**  </br>

核心框架：Spring Framework</br>

视图框架：Spring MVC </br>

持久层框架：MyBatis 3</br>

JS框架：jQuery</br>

富文本：kindeditor</br>

**开发环境**</br>

建议开发者使用以下环境，这样避免版本带来的问题</br>

IDE:eclipse，idea</br>

DB:Mysql5.5</br>

JDK:JAVA 7</br>

tomcat:tomcat 7.0.68已上</br>

**系统美图**

![输入图片说明](http://git.oschina.net/uploads/images/2016/0323/163323_c22814d9_133935.png "首页")

